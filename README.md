# lab56pkg

This ROS package takes in images from a webcam, thresholds them to find blocks against a gridded background, and identifies individual wooden blocks. 
It associates pixels near each other that it assumes belong to the same block, given an algorithm with an average block size and expected darkness range.
Using a pop-up window interface, the code instructs the UR3 robot to pick up blocks on a left click and drop them on a right click.

Most of the functional part of this code exists in lab56func.cpp, while dependencies exist in the remaining files.